# README #

Google places

### Example ###

```php
<?php
$api_key = 'YOUR_API_KEY';
$language = 'ru';
$latitude = 51.509865;
$longitude = -0.118092;
$radius = 20000; // 20 km
$types = 'campground|lodging';

$Fetcher = new Fetcher($api_key, $language);
$Places = new Places($Fetcher);
$result = $Places->findNearest($latitude, $longitude, $radius, $types);
var_dump($result);
```
