<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   2017
 *
 * Google places
 */

namespace alexs\google\places;
use RuntimeException;

class Fetcher
{
    public
        $api_key,
        $language,
        $connection_timeout = 10; // sec

    public function __construct($api_key, $language) {
        $this->api_key = $api_key;
        $this->language = $language;
    }

    /**
     * @param float $lat
     * @param float $lng
     * @param int $radius meters
     * @param string $types google place types
     * @throws RuntimeException
     * @return array
     */
    public function nearBySearch($lat, $lng, $radius, $types) {
        $url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' . $lat . ',' . $lng .
               '&radius=' . $radius . '&types=' . $types . '&key=' . $this->api_key . '&language=' . $this->language;
        $contents = $this->getContents($url);
        if ($contents === false) {
            throw new RuntimeException('An error occurred while getting the content');
        }
        $result = json_decode($contents);
        if ($result->status === 'OK') {
            return $result->results;
        }
        throw new RuntimeException($result->status);
    }

    /**
     * @param string $placeid
     * @throws RuntimeException
     * @return \StdClass
     */
    public function getDetails($placeid) {
        $url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid=' . $placeid . '&key=' . $this->api_key .
               '&language=' . $this->language;
        $contents = $this->getContents($url);
        if ($contents === false) {
            throw new RuntimeException('An error occurred while getting the content');
        }
        $result = json_decode($contents);
        if ($result->status === 'OK') {
            return $result->result;
        }
        throw new RuntimeException($result->status);
    }

    protected function getContents($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->connection_timeout);
        $contents = curl_exec($ch);
        curl_close($ch);
        return $contents;
    }
}