<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   2017
 *
 * Google places
 */

namespace alexs\google\places;

class Places
{
    /** @var Fetcher $Fetcher */
    public $Fetcher;

    /**
     * @var Fetcher $Fetcher
     */
    public function __construct(Fetcher $Fetcher) {
        $this->Fetcher = $Fetcher;
    }

    /**
     * @param float $lat
     * @param float $lng
     * @param int $radius meters
     * @param string $types google place types
     * @return array
     */
    public function findNearest($lat, $lng, $radius, $types) {
        $result = [];
        /** @var \StdClass $places */
        $places = $this->Fetcher->nearBySearch($lat, $lng, $radius, $types);
        if (!empty($places)) {
            foreach ($places as $place) {
                $address = isset($place->vicinity) ? $place->vicinity : ''; // use vicinity by default
                $phone = '';
                $website = '';
                /** @var \StdClass $place_details */
                if ($place_details = $this->Fetcher->getDetails($place->place_id)) {
                    if (isset($place_details->formatted_address)) {
                        $address = $place_details->formatted_address;
                    }
                    if (isset($place_details->international_phone_number)) {
                        $phone = $place_details->international_phone_number;
                    }
                    if (isset($place_details->website)) {
                        $website = rtrim($place_details->website, '/');
                    }
                }
                $result[] = [
                    'lat'    =>$place->geometry->location->lat,
                    'lng'    =>$place->geometry->location->lng,
                    'name'   =>$place->name,
                    'address'=>$address,
                    'phone'  =>$phone,
                    'website'=>$website,
                ];
            }
        }
        return $result;
    }
}