<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\google\places\tests;
use alexs\google\places\Fetcher;
use PHPUnit\Framework\TestCase;

class FetcherTest extends TestCase
{
    /**
     * @expectedException \RuntimeException
     */
    public function testInvalidApiKey() {
        $api_key = 'INVALIDKEY';
        $Fetcher = new Fetcher($api_key, 'ru');
        $Fetcher->getDetails(123);
    }

    /**
     * @expectedException \RuntimeException
     */
    public function testNearBySearchZero() {
        $api_key = 'AIzaSyD7TH4WHO1neEyo5sxw86WesqMI4QP5eTg';
        $latitude = 51.509865;
        $longitude = -0.118092;
        $Fetcher = new Fetcher($api_key, 'ru');
        $Fetcher->nearBySearch($latitude, $longitude, 1, 'airport');
    }

    public function testNearBySearch() {
        $api_key = 'AIzaSyD7TH4WHO1neEyo5sxw86WesqMI4QP5eTg';
        $latitude = 51.509865;
        $longitude = -0.118092;
        $Fetcher = new Fetcher($api_key, 'ru');
        $results = $Fetcher->nearBySearch($latitude, $longitude, 10000, 'airport');
        $this->assertTrue(is_array($results));
    }

    public function testGetDetails() {
        $api_key = 'AIzaSyD7TH4WHO1neEyo5sxw86WesqMI4QP5eTg';
        $placeid = 'ChIJhSzf4jiwEmsRVLD1NuWtDDo';
        $Fetcher = new Fetcher($api_key, 'ru');
        $result = $Fetcher->getDetails($placeid);
        $this->assertInstanceOf('\StdClass', $result);
    }
}