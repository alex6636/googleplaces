<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\google\places\tests;
use alexs\google\places\Places;
use alexs\google\places\Fetcher;
use PHPUnit\Framework\TestCase;

class PlacesTest extends TestCase
{
    /** @var Places $Places */
    protected $Places;

    protected function setUp() {
        $api_key = 'AIzaSyD7TH4WHO1neEyo5sxw86WesqMI4QP5eTg';
        $language = 'ru';
        $Fetcher = new Fetcher($api_key, $language);
        $this->Places = new Places($Fetcher);
    }

    public function testFindNearest() {
        $latitude = 51.509865;
        $longitude = -0.118092;
        $radius = 1000;
        $types = 'airport';
        $results = $this->Places->findNearest($latitude, $longitude, $radius, $types);
        $this->assertInternalType('array', $results);
    }
}